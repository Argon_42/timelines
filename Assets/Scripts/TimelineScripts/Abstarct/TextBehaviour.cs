﻿
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class TextBehaviour : PlayableBehaviour
{
    public Text TextComponent;
    public TimelineClip Clip;
    public PlayableDirector Director;
}
