﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class AnimationBehaviour : TextBehaviour
{
    public string TextValue;

    public override void OnPlayableCreate(Playable playable)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        TextComponent.text = string.Empty;
    }
    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        var color = TextComponent.color;
        color.a = 1;
        TextComponent.color = color;
    }
    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        if (ComponentNotConfigured())
        {
            return;
        }

        if (Director.time > Clip.start)
        {
            TextComponent.text = TextValue;
        }
        else
        {
            TextComponent.text = string.Empty;
        }
    }
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        TextComponent.text = TextValue.Substring(0, (int)(TextValue.Length * (playable.GetPreviousTime() / playable.GetDuration())));
    }

    protected bool ComponentNotConfigured()
    {
        return Clip == null || TextValue == null || TextComponent == null;
    }
}