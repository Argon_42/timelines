﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class UnfadeBehaviour : TextBehaviour
{
    public override void OnPlayableCreate(Playable playable)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        var color = TextComponent.color;
        color.a = 0;
        TextComponent.color = color;
    }
    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        if (ComponentNotConfigured())
        {
            return;
        }

        if (Director.time > Clip.start)
        {
            var color = TextComponent.color;
            color.a = 1;
            TextComponent.color = color;
        }
        else
        {
            var color = TextComponent.color;
            color.a = 0;
            TextComponent.color = color;
        }
    }
    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        var color = TextComponent.color;
        color.a = Mathf.Lerp(0, 1, (float)(playable.GetPreviousTime() / playable.GetDuration()));

        TextComponent.color = color;
    }

    protected bool ComponentNotConfigured()
    {
        return Clip == null || TextComponent == null;
    }
}