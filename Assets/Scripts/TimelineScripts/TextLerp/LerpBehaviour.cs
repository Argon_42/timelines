﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class LerpBehaviour : TextBehaviour
{
    //public string TextValue;
    public Vector2 MinMaxValues;
    public int NumberOfDecimalPlaces;

    public override void OnPlayableCreate(Playable playable)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        TextComponent.text = string.Empty;
    }

    public override void OnBehaviourPlay(Playable playable, FrameData info)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
    }

    public override void OnBehaviourPause(Playable playable, FrameData info)
    {
        if (ComponentNotConfigured())
        {
            return;
        }

        if (Director.time > Clip.start)
        {
            TextComponent.text = MinMaxValues.y.ToString();
        }
        else
        {
            TextComponent.text = MinMaxValues.x.ToString();
        }
    }

    public override void ProcessFrame(Playable playable, FrameData info, object playerData)
    {
        if (ComponentNotConfigured())
        {
            return;
        }
        var currentValue = Mathf.Lerp(MinMaxValues.x, MinMaxValues.y, (float) (playable.GetPreviousTime() / playable.GetDuration()));
        TextComponent.text = System.Math.Round(currentValue, NumberOfDecimalPlaces).ToString();
    }

    protected bool ComponentNotConfigured()
    {
        return Clip == null || TextComponent == null;
    }
}