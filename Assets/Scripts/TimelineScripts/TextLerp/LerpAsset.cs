﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class LerpAsset : TextAsset
{
    [SerializeField] protected ExposedReference<Text> text;
    //[SerializeField, TextArea] private string textValue;
    [SerializeField] private Vector2 MinMaxValues;
    [SerializeField] private int NumberOfDecimalPlaces;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var template = new LerpBehaviour();
        template.TextComponent = text.Resolve(graph.GetResolver());
        //template.TextValue = textValue;
        template.Clip = Clip;
        template.MinMaxValues = MinMaxValues;
        template.NumberOfDecimalPlaces = NumberOfDecimalPlaces;
        template.Director = owner.GetComponent<PlayableDirector>();
        return ScriptPlayable<LerpBehaviour>.Create(graph, template);
    }
}