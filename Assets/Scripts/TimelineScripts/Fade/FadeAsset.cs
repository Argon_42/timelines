﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class FadeAsset : TextAsset
{
    [SerializeField] protected ExposedReference<Text> text;

    public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
    {
        var template = new FadeBehaviour();
        template.TextComponent = text.Resolve(graph.GetResolver());
        template.Clip = Clip;
        template.Director = owner.GetComponent<PlayableDirector>();
        return ScriptPlayable<FadeBehaviour>.Create(graph, template);
    }
}